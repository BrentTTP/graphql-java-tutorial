package company.tothepoint.graphql.authentication;

import company.tothepoint.graphql.model.Player;
import graphql.servlet.GraphQLContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

public class AuthContext extends GraphQLContext {

    private final Player player;

    public AuthContext(Player player, Optional<HttpServletRequest> request, Optional<HttpServletResponse> response) {
        super(request, response);
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
