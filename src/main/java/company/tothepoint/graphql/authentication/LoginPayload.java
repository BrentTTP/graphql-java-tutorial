package company.tothepoint.graphql.authentication;

import company.tothepoint.graphql.model.Player;

public class LoginPayload {

    private final String token;
    private final Player player;

    public LoginPayload(String token, Player player) {
        this.token = token;
        this.player = player;
    }

    public String getToken() {
        return token;
    }

    public Player getPlayer() {
        return player;
    }
}
