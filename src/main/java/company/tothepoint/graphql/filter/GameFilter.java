package company.tothepoint.graphql.filter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GameFilter {

    private String genreIs;
    private String titleContains;

    @JsonProperty("genre_is")
    public String getGenreIs() {
        return genreIs;
    }

    public void setGenreIs(String genreIs) {
        this.genreIs = genreIs;
    }

    @JsonProperty("title_contains")
    public String getTitleContains() {
        return titleContains;
    }

    public void setTitleContains(String titleContains) {
        this.titleContains = titleContains;
    }
}
