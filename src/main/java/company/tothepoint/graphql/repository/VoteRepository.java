package company.tothepoint.graphql.repository;

import com.mongodb.client.MongoCollection;
import company.tothepoint.graphql.model.Scalars;
import company.tothepoint.graphql.model.Vote;
import org.bson.Document;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class VoteRepository {

    private final MongoCollection<Document> votes;

    public VoteRepository(MongoCollection<Document> votes) {
        this.votes = votes;
    }

    public List<Vote> findByPlayerId(String playerId) {
        List<Vote> list = new ArrayList<>();
        for (Document doc : votes.find(eq("playerId", playerId))) {
            list.add(vote(doc));
        }
        return list;
    }

    public Vote saveVote(Vote vote) {
        Document doc = new Document();
        doc.append("playerId", vote.getPlayerId());
        doc.append("gameId", vote.getGameId());
        doc.append("createdAt", Scalars.dateTime.getCoercing().serialize(vote.getCreatedAt()));
        votes.insertOne(doc);
        return new Vote(
                doc.get("_id").toString(),
                ZonedDateTime.parse(doc.getString("createdAt")),
                doc.getString("playerId"),
                doc.getString("gameId"));
    }

    private Vote vote(Document doc) {
        return new Vote(
                doc.get("_id").toString(),
                ZonedDateTime.parse(doc.getString("createdAt")),
                doc.getString("playerId"),
                doc.getString("gameId"));
    }
}
