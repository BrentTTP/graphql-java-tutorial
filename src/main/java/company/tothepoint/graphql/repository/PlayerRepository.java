package company.tothepoint.graphql.repository;

import com.mongodb.client.MongoCollection;
import company.tothepoint.graphql.model.Player;
import org.bson.Document;
import org.bson.types.ObjectId;

import static com.mongodb.client.model.Filters.eq;

public class PlayerRepository {

    private final MongoCollection<Document> players;

    public PlayerRepository(MongoCollection<Document> players) {
        this.players = players;
    }

    public Player findByEmail(String email) {
        Document doc = players.find(eq("email", email)).first();
        return player(doc);
    }

    public Player findById(String id) {
        Document doc = players.find(eq("_id", new ObjectId(id))).first();
        return player(doc);
    }

    public Player savePlayer(Player player) {
        Document doc = new Document();
        doc.append("name", player.getName());
        doc.append("email", player.getEmail());
        doc.append("password", player.getPassword());
        players.insertOne(doc);
        return new Player(
                doc.get("_id").toString(),
                player.getName(),
                player.getEmail(),
                player.getPassword());
    }

    private Player player(Document doc) {
        if (doc == null) {
        return null;
        }

        return new Player(
                doc.get("_id").toString(),
                doc.getString("name"),
                doc.getString("email"),
                doc.getString("password"));
    }
}
