package company.tothepoint.graphql.repository;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import company.tothepoint.graphql.filter.GameFilter;
import company.tothepoint.graphql.model.Game;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.regex;

public class GameRepository {

    private final MongoCollection<Document> games;

    public GameRepository(MongoCollection<Document> games) {
        this.games = games;
    }

    public Game findById(String id) {
        Document doc = games.find(eq("_id", new ObjectId(id))).first();
        return game(doc);
    }

    public List<Game> getAllGames(GameFilter filter, int skip, int first) {
        Optional<Bson> mongoFilter = Optional.ofNullable(filter).map(this::buildFilter);

        List<Game> allGames = new ArrayList<>();
        FindIterable<Document> documents = mongoFilter.map(games::find).orElseGet(games::find);
        for (Document doc : documents.skip(skip).limit(first)) {
            allGames.add(game(doc));
        }
        return allGames;
    }

    private Bson buildFilter(GameFilter filter) {
        String genrePattern = filter.getGenreIs();
        String titlePattern = filter.getTitleContains();

        Bson genreCondition = null;
        Bson titleCondition= null;

        if (genrePattern != null && !genrePattern.isEmpty()) genreCondition = regex("genre", ".*" + genrePattern + ".*", "i");
        if (titlePattern != null && !titlePattern.isEmpty()) titleCondition = regex("title", ".*" + titlePattern + ".*", "i");

        if (genreCondition != null && titleCondition != null) return and(genreCondition, titleCondition);

        return genreCondition != null ? genreCondition : titleCondition;
    }

    public void saveGame(Game game) {
        Document doc = new Document();
        doc.append("title", game.getTitle());
        doc.append("genre", game.getGenre());
        doc.append("playedBy", game.getPlayerId());
        games.insertOne(doc);
    }

    private Game game(Document doc) {
        return new Game(
                doc.get("_id").toString(),
                doc.getString("title"),
                doc.getString("genre"),
                doc.getString("playedBy"));
    }
}
