package company.tothepoint.graphql;

import com.coxautodev.graphql.tools.SchemaParser;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import company.tothepoint.graphql.authentication.AuthContext;
import company.tothepoint.graphql.exception.SanitizedError;
import company.tothepoint.graphql.model.Player;
import company.tothepoint.graphql.model.Scalars;
import company.tothepoint.graphql.repository.PlayerRepository;
import company.tothepoint.graphql.repository.VoteRepository;
import company.tothepoint.graphql.resolver.complex.GameResolver;
import company.tothepoint.graphql.resolver.complex.LoginResolver;
import company.tothepoint.graphql.resolver.Mutation;
import company.tothepoint.graphql.resolver.Query;
import company.tothepoint.graphql.repository.GameRepository;
import company.tothepoint.graphql.resolver.complex.VoteResolver;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.schema.GraphQLSchema;
import graphql.servlet.GraphQLContext;
import graphql.servlet.SimpleGraphQLServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@WebServlet(urlPatterns = "/graphql")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

    private static final GameRepository gameRepository;
    private static final PlayerRepository playerRepository;
    private static VoteRepository voteRepository;


    static {
        MongoDatabase mongo = new MongoClient().getDatabase("graphql-games");
        gameRepository = new GameRepository(mongo.getCollection("games"));
        playerRepository = new PlayerRepository(mongo.getCollection("players"));
        voteRepository = new VoteRepository(mongo.getCollection("votes"));
    }

    public GraphQLEndpoint() {
        super(buildSchema());
    }

    private static GraphQLSchema buildSchema() {
        return SchemaParser.newParser()
                .file("schema.graphqls")
                .resolvers(
                        new Query(gameRepository),
                        new Mutation(gameRepository, playerRepository, voteRepository),
                        new LoginResolver(),
                        new GameResolver(playerRepository),
                        new VoteResolver(gameRepository, playerRepository))
                .scalars(Scalars.dateTime)
                .build()
                .makeExecutableSchema();
    }

    @Override
    protected GraphQLContext createContext(Optional<HttpServletRequest> request, Optional<HttpServletResponse> response) {
        Player player = request
                .map(req -> req.getHeader("Authorization"))
                .filter(id -> !id.isEmpty())
                .map(id -> id.replace("Bearer ", ""))
                .map(playerRepository::findById)
                .orElse(null);
        return new AuthContext(player, request, response);
    }

    @Override
    protected List<GraphQLError> filterGraphQLErrors(List<GraphQLError> errors) {
        return errors.stream()
                .filter(e -> e instanceof ExceptionWhileDataFetching || super.isClientError(e))
                .map(e -> e instanceof ExceptionWhileDataFetching ? new SanitizedError((ExceptionWhileDataFetching) e) : e)
                .collect(Collectors.toList());
    }
}
