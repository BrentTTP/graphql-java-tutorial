package company.tothepoint.graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLRootResolver;
import company.tothepoint.graphql.filter.GameFilter;
import company.tothepoint.graphql.model.Game;
import company.tothepoint.graphql.repository.GameRepository;

import java.util.List;

public class Query implements GraphQLRootResolver {

    private final GameRepository gameRepository;

    public Query(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public List<Game> allGames(GameFilter filter, Number skip, Number first) {
        return gameRepository.getAllGames(filter, skip.intValue(), first.intValue());
    }
}
