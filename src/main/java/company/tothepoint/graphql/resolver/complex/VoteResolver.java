package company.tothepoint.graphql.resolver.complex;

import com.coxautodev.graphql.tools.GraphQLResolver;
import company.tothepoint.graphql.model.Game;
import company.tothepoint.graphql.model.Player;
import company.tothepoint.graphql.model.Vote;
import company.tothepoint.graphql.repository.GameRepository;
import company.tothepoint.graphql.repository.PlayerRepository;

public class VoteResolver implements GraphQLResolver<Vote> {

    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;

    public VoteResolver(GameRepository gameRepository, PlayerRepository playerRepository) {
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
    }

    public Player player(Vote vote) {
        return playerRepository.findById(vote.getPlayerId());
    }

    public Game game(Vote vote) {
        return gameRepository.findById(vote.getGameId());
    }
}
