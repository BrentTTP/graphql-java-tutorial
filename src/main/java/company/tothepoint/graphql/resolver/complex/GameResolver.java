package company.tothepoint.graphql.resolver.complex;

import com.coxautodev.graphql.tools.GraphQLResolver;
import company.tothepoint.graphql.model.Game;
import company.tothepoint.graphql.model.Player;
import company.tothepoint.graphql.repository.PlayerRepository;

public class GameResolver implements GraphQLResolver<Game> {

    private final PlayerRepository playerRepository;

    public GameResolver(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Player playedBy(Game game) {
        if (game.getPlayerId() == null)
            return null;
        return playerRepository.findById(game.getPlayerId());
    }
}
