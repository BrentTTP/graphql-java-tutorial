package company.tothepoint.graphql.resolver.complex;

import com.coxautodev.graphql.tools.GraphQLResolver;
import company.tothepoint.graphql.authentication.LoginPayload;
import company.tothepoint.graphql.model.Player;

public class LoginResolver implements GraphQLResolver<LoginPayload> {

    public Player player(LoginPayload payload) {
        return payload.getPlayer();
    }
}
