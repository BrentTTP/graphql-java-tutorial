package company.tothepoint.graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLRootResolver;
import company.tothepoint.graphql.authentication.AuthContext;
import company.tothepoint.graphql.authentication.AuthData;
import company.tothepoint.graphql.authentication.LoginPayload;
import company.tothepoint.graphql.model.Game;
import company.tothepoint.graphql.model.Player;
import company.tothepoint.graphql.model.Vote;
import company.tothepoint.graphql.repository.GameRepository;
import company.tothepoint.graphql.repository.PlayerRepository;
import company.tothepoint.graphql.repository.VoteRepository;
import graphql.GraphQLException;
import graphql.schema.DataFetchingEnvironment;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class Mutation implements GraphQLRootResolver {

    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;
    private final VoteRepository voteRepository;

    public Mutation(GameRepository gameRepository, PlayerRepository playerRepository, VoteRepository voteRepository) {
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
        this.voteRepository = voteRepository;
    }

    public Game createGame(String title, String genre, DataFetchingEnvironment env) {
        AuthContext context = env.getContext();
        Game newGame = new Game(title, genre, context.getPlayer().getId());
        gameRepository.saveGame(newGame);
        return newGame;
    }

    public Player createPlayer(String name, AuthData auth) {
        Player newPlayer = new Player(name, auth.getEmail(), auth.getPassword());
        return playerRepository.savePlayer(newPlayer);
    }

    public LoginPayload loginPlayer(AuthData auth) throws IllegalAccessException {
        Player player = playerRepository.findByEmail(auth.getEmail());
        if (player.getPassword().equals(auth.getPassword())) {
            return new LoginPayload(player.getId(), player);
        }
        throw new GraphQLException("Invalid credentials");
    }

    public Vote createVote(String gameId, String playerId) {
        ZonedDateTime now = Instant.now().atZone(ZoneOffset.UTC);
        return voteRepository.saveVote(new Vote(now, playerId, gameId));
    }
}
