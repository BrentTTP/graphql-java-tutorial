package company.tothepoint.graphql.model;

public class Player {

    private final String id;
    private final String name;
    private final String email;
    private final String password;

    public Player(String name, String email, String password) {
        this(null, name, email, password);
    }

    public Player(String id, String name, String email, String password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
