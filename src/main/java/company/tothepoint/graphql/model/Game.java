package company.tothepoint.graphql.model;

public class Game {

    private final String id;
    private final String title;
    private final String genre;
    private final String playerId;

    public Game(String title, String genre, String playerId) {
        this(null, title, genre, playerId);
    }

    public Game(String id, String title, String genre, String playerId) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.playerId = playerId;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getPlayerId() {
        return playerId;
    }
}
