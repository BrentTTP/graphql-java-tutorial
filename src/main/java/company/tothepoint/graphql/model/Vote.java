package company.tothepoint.graphql.model;

import java.time.ZonedDateTime;

public class Vote {

    private final String id;
    private final ZonedDateTime createdAt;
    private final String playerId;
    private final String gameId;

    public Vote(ZonedDateTime createdAt, String playerId, String gameId) {
        this(null, createdAt, playerId, gameId);
    }

    public Vote(String id, ZonedDateTime createdAt, String playerId, String gameId) {
        this.id = id;
        this.createdAt = createdAt;
        this.playerId = playerId;
        this.gameId = gameId;
    }

    public String getId() {
        return id;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getGameId() {
        return gameId;
    }
}
