# GraphQL Java Tutorial Project
## GraphQL
> GraphQL is a query language for APIs and a runtime for fulfilling those 
> queries with your existing data. GraphQL provides a complete and 
> understandable description of the data in your API, gives clients the power 
> to ask for exactly what they need and nothing more, makes it easier to 
> evolve APIs over time, and enables powerful developer tools. - graphql.github.io

## Components
#### Schema

The schema is the core of a GraphQL implementation and can be defined in two ways:
* Programmatically (type definitions in code)
* Using the Schema Definition Language (SDL) - schema generated from a textual language-independent description 
(loosely resembling JSON)

A schema has the proper file extension: `.graphqls`

And looks something like:

- Types representing a game and a player
```
type Game {
    id: ID!
    title: String!
    genre: String!
    bestPlayer: Player
}
type Player {
    id: ID!
    name: String!
}
```
- A Query to fetch all games 
```
query Query {
    allGames: [Game]
}
```
- The schema containing this query
```
schema {
    query: Query
}
```

In GraphQL, as in most languages, you have primitive and complex types,
GraphQL only has 5 primitive types: Int, Float, String, ID and Boolean.

To ensure the requested field in properly filled you can make a field
non-nullable by adding a `!` at the end.

Lists are also a part of GraphQL of course, this can be accomplished by
putting the type between square brackets `[`{type}`]`

#### GraphQL Endpoint
In contrary to a classic REST approach, GraphQL does not require an endpoint
for every GET or POST request. Alternatively GraphQL offers a single endpoint
where a query parameter is required that contains all the necessary information
to process the request (be it what object you want and which field of that object,
what object you want to update or delete). The possibilities are only limited by
what you can imagine to put in the query.

A simple `GraphQLEndpoint` class does the trick:
```
@WebServlet(urlPatterns = "/graphql")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

    public GraphQLEndpoint() {
        super(SchemaParser.newParser()
                .file("schema.graphqls")
                .build()
                .makeExecutableSchema());
    }
}
```

#### Resolvers (Query & Mutation)
To be able to actually use the queries and mutations defined in the schema you need classes
who represent them, this is where you model the behavior for example the allGames query:

```
public class Query implements GraphQLRootResolver {
    
    private final GameRepository gameRepository;

    public Query(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public List<Game> allGames() {
        return gameRepository.getAllGames();
    }
}
```

As you can see we have a simple `Query` class with an `allGames()` method that returns 
a list of games from some repository.

Now all we have to do is add the resolver to the GraphQLEndpoint and we're good to go:
```
@WebServlet(urlPatterns = "/graphql")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

    public GraphQLEndpoint() {
        super(buildSchema());
    }

    private static GraphQLSchema buildSchema() {
        GameRepository gameRepository = new GameRepository();
        return SchemaParser.newParser()
                .file("schema.graphqls")
                .resolvers(new Query(gameRepository)) <- !!!
                .build()
                .makeExecutableSchema();
    }
}
```
#### Run it!
To start up the project just run it with `mvn jetty:run`, now you can either put your
query directly in the URL or with Graph*i*QL, an in-browser IDE to fire queries and 
mutations
#### In URL
To get all the games (specifically their titles) from our database we can construct a query like:
```
{
    allGames {
        title
    }
}
```
To pass it in the URL it would look something liks:
`localhost:8080/?query={allGames{title}}`

But of course we can make our testing lives much easier with a simple tool to construct
and run queries.
#### Try with Graph*i*QL
To use Graph*i*QL all you need to do is add a prefab `index.html` file and go to 
`localhost:8080`, normally you should see something like:
![GraphiQL Screenshot](https://i.imgur.com/KlnKaZe.png)

Just add your query in the input field and press play to get the resulting output:
![GraphiQL custom query screenshot](https://i.imgur.com/aNhC7E6.png)